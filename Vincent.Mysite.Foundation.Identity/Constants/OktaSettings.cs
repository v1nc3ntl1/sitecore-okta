﻿
namespace Vincent.Foundation.Identity.Common
{
    public static class OktaSettings
    {
        public static readonly string ClientId = "Vincent.Mysite.Foundation.Identity.Okta.ClientId";
        public static readonly string ClientSecret = "Vincent.Mysite.Foundation.Identity.Okta.ClientSecret";
        public static readonly string Authority = "Vincent.Mysite.Foundation.Identity.Okta.Authority";
        public static readonly string OAuthTokenEndpoint = "Vincent.Mysite.Foundation.Identity.Okta.OAuthTokenEndpoint";
        public static readonly string OAuthUserInfoEndpoint = "Vincent.Mysite.Foundation.Identity.Okta.OAuthUserInfoEndpoint";
        public static readonly string OAuthRedirectUri = "Vincent.Mysite.Foundation.Identity.Okta.OAuthRedirectUri";
        public static readonly string ApiKey = "Vincent.Mysite.Foundation.Identity.Okta.ApiKey";
    }
}