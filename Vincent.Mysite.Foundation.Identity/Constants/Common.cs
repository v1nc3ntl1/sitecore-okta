﻿namespace Vincent.Foundation.Identity.Constants
{
    public class Common
    {
        public readonly static string CorrelationId = "CorrelationId";
        public readonly static string GroupClaimSeparator = ";";
    }
}
