﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Sitecore.Owin.Authentication.Configuration;
using Sitecore.Owin.Authentication.Identity;
using Sitecore.Owin.Authentication.Services;
using Sitecore.SecurityModel.Cryptography;
using System;
using System.Linq;

namespace Vincent.Foundation.Identity.Providers
{
    public class UserBuilder : DefaultExternalUserBuilder
    {
        public UserBuilder(ApplicationUserFactory applicationUserFactory, IHashEncryption hashEncryption) : base(applicationUserFactory, hashEncryption)
        {
        }

        protected override string CreateUniqueUserName(UserManager<ApplicationUser> userManager, ExternalLoginInfo externalLoginInfo)
        {
            IdentityProvider identityProvider = this.FederatedAuthenticationConfiguration.GetIdentityProvider(externalLoginInfo.ExternalIdentity);
            if (identityProvider == null)
                throw new InvalidOperationException("Unable to retrieve identity provider for given identity");

            var username = "";
            string domain = identityProvider.Domain;

            var userNameClaim = externalLoginInfo.ExternalIdentity.Claims.FirstOrDefault(c => c.Type.Equals("preferred_username"));

            if (userNameClaim != null)
            {
                username = userNameClaim.Value;
                return domain + "\\" + username;
            }

            userNameClaim = externalLoginInfo.ExternalIdentity.Claims.FirstOrDefault(c => c.Type.Equals("given_name"));

            if (userNameClaim != null)
            {
                username = userNameClaim.Value;
                return domain + "\\" + username;
            }

            userNameClaim = externalLoginInfo.ExternalIdentity.Claims.FirstOrDefault(c => c.Type.Equals("name"));

            if (userNameClaim != null)
            {
                username = userNameClaim.Value;
                return domain + "\\" + username;
            }

            userNameClaim = externalLoginInfo.ExternalIdentity.Claims.FirstOrDefault(c => c.Type.Equals("email"));

            if (userNameClaim != null)
            {
                username = userNameClaim.Value;
                return domain + "\\" + username;
            }

            return domain + "\\" + Guid.NewGuid().ToString();
        }

        public override ApplicationUser BuildUser(UserManager<ApplicationUser> userManager, ExternalLoginInfo externalLoginInfo)
        {
            IdentityProvider identityProvider = this.FederatedAuthenticationConfiguration.GetIdentityProvider(externalLoginInfo.ExternalIdentity);
            if (identityProvider == null)
                throw new InvalidOperationException("Unable to retrieve identity provider for given identity");

            string userName = CreateUniqueUserName(userManager, externalLoginInfo);
            ApplicationUser applicationUser = ApplicationUserFactory.CreateUser(userName);
            applicationUser.IsVirtual = !IsPersistentUser;

            return applicationUser;
        }
    }
}