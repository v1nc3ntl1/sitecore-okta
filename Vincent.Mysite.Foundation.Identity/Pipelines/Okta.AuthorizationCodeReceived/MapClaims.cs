﻿using Sitecore.Diagnostics;
using Sitecore.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Vincent.Foundation.Identity.Services;

namespace Vincent.Foundation.Identity.Pipelines.Okta.AuthorizationCodeReceived
{
    public class MapClaims
    {
        public void Process(OktaPostAuthorizationArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            Assert.ArgumentNotNull(args.UserInfo, "args.UserInfo");
            Assert.ArgumentNotNull(args.UserInfo.Claims, "args.UserInfo.Claims");
            Assert.ArgumentNotNull(args.Token, "args.Token");
            Assert.ArgumentNotNull(args.Notification, "args.Notification");

            var claims = new List<Claim>();
            claims.AddRange(args.UserInfo.Claims);
            claims.Add(new Claim("id_token", args.Token.IdentityToken));
            claims.Add(new Claim("access_token", args.Token.AccessToken));

            if (!string.IsNullOrEmpty(args.Token.RefreshToken))
            {
                claims.Add(new Claim("refresh_token", args.Token.RefreshToken));
            }

            args.ClaimName = GetClaimsName(args);

            AddGroupsToClaim(args, claims);

            if (args.Aborted) return;


            args.Notification.AuthenticationTicket.Identity.AddClaims(claims);
        }

        private void AddGroupsToClaim(OktaPostAuthorizationArgs args, List<Claim> claims)
        {
            string userId = claims.FirstOrDefault(c => string.Equals(c.Type, "sub", System.StringComparison.InvariantCultureIgnoreCase))?.Value;

            if (string.IsNullOrWhiteSpace(userId))
            {
                Log.Error($"Failed to identify which role user belonging to. Missing 'sub' key in claims. \nCorrelation Id: {WebUtil.GetCookieValue(Constants.Common.CorrelationId)}", this);
                return;
            }

            IEnumerable<string> groupNames = OktaClient.GetGroupNamesForUser(userId);

            Log.Info($"Group return from Okta /api/v1/users/<userId>/groups endpoint. content: {string.Join(",", groupNames)}\nCorrelation Id: {WebUtil.GetCookieValue(Constants.Common.CorrelationId)}", this);

            if (groupNames == null || !groupNames.Any())
            {
                Log.Info($"Logging out user from portal. Failed to identify which role user belonging to. Not group return from Okta /api/v1/users/<userId>/groups endpoint. \nCorrelation Id: {WebUtil.GetCookieValue(Constants.Common.CorrelationId)}", this);
                args.Notification.HandleResponse();
                args.Notification.Response.Redirect("/mysite");
                args.AbortPipeline();
                return;
            }

            var groupClaim = string.Join(Constants.Common.GroupClaimSeparator, groupNames);

            claims.Add(new Claim("groups", groupClaim));
        }

        private string GetClaimsName(OktaPostAuthorizationArgs args)
        {
            var name = args.UserInfo.Claims.FirstOrDefault(c => c.Type?.ToLower() == "name");

            if (name != null && !string.IsNullOrEmpty(name.Value)) return name.Value;

            name = args.UserInfo.Claims.FirstOrDefault(c => c.Type?.ToLower() == "preferred_username");

            if (name != null && !string.IsNullOrEmpty(name.Value)) return name.Value;

            name = args.UserInfo.Claims.FirstOrDefault(c => c.Type?.ToLower() == "email");

            if (name != null && !string.IsNullOrEmpty(name.Value)) return name.Value;

            name = args.UserInfo.Claims.FirstOrDefault(c => c.Type?.ToLower() == "given_name");

            if (name != null && !string.IsNullOrEmpty(name.Value)) return name.Value;

            return string.Empty;
        }
    }
}
