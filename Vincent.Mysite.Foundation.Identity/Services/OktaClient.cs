﻿using Newtonsoft.Json;
using Sitecore.Configuration;
using Sitecore.Diagnostics;
using Sitecore.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using Vincent.Foundation.Identity.Models;

namespace Vincent.Foundation.Identity.Services
{
    public class OktaClient
    {
        private static readonly HttpClient _httpClient;

        private static string Authority => Settings.GetSetting(OktaSettings.Authority, "");
        private const string GroupsEndpoint = "/api/v1/users/{0}/groups";
        private static readonly string Domain = "mysite";
        private static string OktaAPIKey => Settings.GetSetting(OktaSettings.ApiKey, "");

        static OktaClient()
        {
            _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("SSWS", OktaAPIKey);
        }

        public static IEnumerable<string> GetGroupNamesForUser(string userId)
        {
            var formattedGroupsEndpoint = string.Format(GroupsEndpoint, userId);

            GroupInfo groups = null;

            try
            {
                var response = _httpClient.GetAsync($"{Authority}{formattedGroupsEndpoint}").Result;

                Log.Info($"{Authority}{formattedGroupsEndpoint} endpoint is giving response.IsSuccessStatusCode = {response.IsSuccessStatusCode} \nCorrelation Id: {WebUtil.GetCookieValue(Constants.Common.CorrelationId)}", typeof(OktaClient));

                if (!response.IsSuccessStatusCode) return null;

                var content = response.Content.ReadAsStringAsync().Result;

                if (string.IsNullOrWhiteSpace(content)) return null;

                Log.Debug($"Calling {Authority}{formattedGroupsEndpoint} endpoint is returning response content below. \n{content} \nCorrelation Id: {WebUtil.GetCookieValue(Constants.Common.CorrelationId)}", typeof(OktaClient));

                groups = JsonConvert.DeserializeObject<GroupInfo>(content);
            }
            catch(Exception ex)
            {
                Log.Error($"Calling {Authority}{formattedGroupsEndpoint} endpoint failed. \nError Message: {ex.ToString()} \nCorrelation Id: {WebUtil.GetCookieValue(Constants.Common.CorrelationId)}", typeof(OktaClient));
                return null;
            }

            if (groups == null || !groups.Any()) return null;

            return groups.Where(gi => gi != null && 
                                    gi.Profile != null && 
                                    gi.Profile.Name != null)?
                .Select(g => $"{Domain}\\{g.Profile.Name.Replace("_", " ".Replace("-", ""))}");
        }
    }
}