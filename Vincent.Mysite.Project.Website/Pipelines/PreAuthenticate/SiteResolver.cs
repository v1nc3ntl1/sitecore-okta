﻿// Sitecore.Pipelines.PreAuthenticateRequest.SiteResolver
using Sitecore;
using Sitecore.Abstractions;
using Sitecore.Configuration.KnownSettings;
using Sitecore.Diagnostics;
using Sitecore.IO;
using Sitecore.Pipelines.HttpRequest;
using Sitecore.Sites;
using Sitecore.Web;
using System;
using System.IO;
using System.Web;

namespace Vincent.Mysite.Project.Website.Pipelines.PreAuthenticate
{
	/// <summary>
	/// Resolves a site.
	/// </summary>
	/// <seealso cref="T:Sitecore.Pipelines.PreAuthenticateRequest.PreAuthenticateRequestProcessor" />
	public class SiteResolver : HttpRequestProcessor
	{
		/// <summary>
		/// Site context factory.
		/// </summary>
		protected BaseSiteContextFactory SiteContextFactory
		{
			get;
		}

		/// <summary>
		/// Gets the settings.
		/// </summary>
		/// <value>
		/// The settings.
		/// </value>
		protected BaseSettings Settings
		{
			get;
		}

		/// <summary>
		/// Gets the HTTP context.
		/// </summary>
		/// <value>
		/// The HTTP context.
		/// </value>
		protected virtual HttpContextBase HttpContext => new HttpContextWrapper(System.Web.HttpContext.Current);

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Sitecore.Pipelines.PreAuthenticateRequest.SiteResolver" /> class.
		/// </summary>
		/// <param name="siteContextFactory">The site context factory.</param>
		/// <param name="settings">The settings.</param>
		public SiteResolver(BaseSiteContextFactory siteContextFactory, BaseSettings settings)
		{
			SiteContextFactory = siteContextFactory;
			Settings = settings;
		}

		/// <inheritdoc />
		public override void Process(HttpRequestArgs args)
		{
			SiteContext siteContext2 = Context.Site = ResolveSiteContext(args);
		}

		/// <summary>
		/// Resolves the site context.
		/// </summary>
		/// <param name="args">The args.</param>
		/// <returns></returns>
		protected virtual SiteContext ResolveSiteContext(HttpRequestArgs args)
		{
			string queryString = WebUtil.GetQueryString("sc_site", string.Empty, HttpContext);
			SiteContext siteContext;
			if (queryString.Length > 0)
			{
				siteContext = SiteContextFactory.GetSiteContext(queryString);
				Assert.IsNotNull(siteContext, "Site from query string was not found: " + queryString);
				return siteContext;
			}
			if (Settings.Core().EnableSiteConfigFiles)
			{
				string path = Path.GetDirectoryName(args.Url.FilePath) ?? string.Empty;
				string part = FileUtil.NormalizeWebPath(path);
				string text = FileUtil.MakePath(part, "site.config");
				if (FileUtil.Exists(text))
				{
					siteContext = SiteContextFactory.GetSiteContextFromFile(text);
					Assert.IsNotNull(siteContext, "Site from site.config was not found: " + text);
					return siteContext;
				}
			}
			Uri uri = Assert.ResultNotNull(HttpContext?.Request?.Url);
			siteContext = SiteContextFactory.GetSiteContext(uri.Host, args.Url.FilePath, uri.Port);
			Assert.IsNotNull(siteContext, "Site from host name and path was not found. Host: " + uri.Host + ", path: " + args.Url.FilePath);
			return siteContext;
		}
	}

}