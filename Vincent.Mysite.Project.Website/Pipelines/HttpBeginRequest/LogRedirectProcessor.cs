﻿using Sitecore;
using Sitecore.Configuration;
using Sitecore.Diagnostics;
using Sitecore.Layouts;
using Sitecore.Pipelines.HttpRequest;
using Sitecore.Sites;
using Sitecore.Web;
using System.Collections.Generic;
using System.Web;

namespace Vincent.Mysite.Project.Website.Pipelines.HttpBeginRequest
{
    public class LogRedirectProcessor : HttpRequestProcessor
    {
        /// <summary>
        /// Pipeline processor implementation.
        /// </summary>
        /// <param name="args">Pipeline processor arguments.</param>
        public override void Process(HttpRequestArgs args)
        {
            if (!HttpContext.Current.Request.Url.PathAndQuery.TrimStart('/').StartsWith(Constants.Common.SiteName)) return;

            if (Sitecore.Context.Item != null) return;

            SiteContext site = Context.Site;
            if (((site != null) && !SiteManager.CanEnter(site.Name, Context.User)))
            {
                // Fall through to default handler
                return;
            }

            PageContext page = Context.Page;
            Assert.IsNotNull(page, "No page context in processor.");
            string filePath = page.FilePath;

            if (filePath.Length > 0)
            {
                if (WebUtil.IsExternalUrl(filePath))
                {
                    WebUtil.Redirect(filePath, true);
                }
                else
                {
                    HttpContext.Current.RewritePath(filePath, HttpContext.Current.Request.PathInfo, args.Url.QueryString, false);
                }
            }
            else if (Context.Item == null)
            {
                this.HandleItemNotFound(args);
            }
        }

        private void HandleItemNotFound(HttpRequestArgs args)
        {
            string localPath = args.LocalPath;
            string name = Context.User.Name;
            bool flag = false;
            string itemNotFoundUrl = Settings.ItemNotFoundUrl;

            if (args.PermissionDenied)
            {
                // redirect to login
                WebUtil.Redirect($"/identity/login/{Context.Site.Name}/Okta");
            }

            string str4 = (Context.Site != null) ? Context.Site.Name : string.Empty;
            List<string> list = new List<string>(new string[] { "item", localPath, "user", name, "site", str4 });

            if (Settings.Authentication.SaveRawUrl)
            {
                list.AddRange(new string[] { "url", HttpUtility.UrlEncode(Context.RawUrl) });
            }

            itemNotFoundUrl = WebUtil.AddQueryString(itemNotFoundUrl, list.ToArray());


            if (Context.Database == null)
            {
                // Fall through to default handler
                return;
            }

            // Find context site root
            var root = args.GetItem(Context.Site.StartPath);
            if (root == null)
            {
                // Fall through to default handler
                return;
            }

            // Find context site pagenotfound pages // Must be located straight under the root 
        }
    }
}