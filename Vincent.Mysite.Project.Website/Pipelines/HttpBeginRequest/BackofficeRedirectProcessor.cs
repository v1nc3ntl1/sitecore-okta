﻿
using Sitecore.Diagnostics;
using Sitecore.Pipelines.HttpRequest;
using Sitecore.Web;
using System.Text.RegularExpressions;
using System.Web;

namespace Vincent.Mysite.Project.Website.Pipelines.HttpBeginRequest
{
    public class BackofficeRedirectProcessor : HttpRequestProcessor
    {
        public override void Process(HttpRequestArgs args)
        {
            Log.Debug($"Executing {this.GetType().FullName}.", this);

            var isBOPortalLoginPageRegex = new Regex($"{Constants.Common.SiteName}(/backoffice|/login){{1,2}}(/)?$", RegexOptions.IgnoreCase);

            if (isBOPortalLoginPageRegex.Matches(HttpContext.Current.Request.Url.AbsoluteUri).Count == 0) return;

            Log.Debug($"Executed {this.GetType().FullName}.", this);

            WebUtil.Redirect($"/{Constants.Common.SiteName}/backoffice/dashboard", false);
        }
    }
}